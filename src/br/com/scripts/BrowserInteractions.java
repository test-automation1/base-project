package br.com.scripts;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BrowserInteractions {	
	
public WebDriver driver;	
	
	public BrowserInteractions(WebDriver driver){
		this.driver = driver;
	}
	
/*############################################################################################ CLICK ###########################################################################################*/
	
	public void clickFieldByID(String id){
		driver.findElement(By.id(id)).click();
	}
	
	
	public void clickFieldByName(String name){
		driver.findElement(By.name(name)).click();
	}
	
	
	public void clickFieldByCssSelector(String cssSelector){
		driver.findElement(By.cssSelector(cssSelector)).click();
	}	
	
	
	public void clickFieldByXpath(String xpath){
		driver.findElement(By.xpath(xpath)).click();
	}
	
	
/*########################################################################################## SET VALUE #######################################################################################*/
	
	public void setValueByID(String id, String value){
		driver.findElement(By.id(id)).sendKeys(value);
	}
	
	
	public void setValueByName(String name, String value){
		driver.findElement(By.name(name)).sendKeys(value);
	}
	
	
	public void setValueByCssSelector(String cssSelector, String value){
		driver.findElement(By.cssSelector(cssSelector)).sendKeys(value);
	}
	
	
	public void setValueByXpath(String xpath, String value){
		driver.findElement(By.xpath(xpath)).sendKeys(value);
	}	
	
	
/*###################################################################################### CLICK AND SET VALUE ###########################################################################################*/
	
	public void clickAndSetByID(String id, String value){
		driver.findElement(By.id(id)).click();
		driver.findElement(By.id(id)).sendKeys(value);
	}
	
	
	public void clickAndSetByName(String name, String value){
		driver.findElement(By.name(name)).click();
		driver.findElement(By.name(name)).sendKeys(value);
	}
	
	
	public void clickAndSetByCssSelector(String cssSelector, String value){
		driver.findElement(By.cssSelector(cssSelector)).click();
		driver.findElement(By.cssSelector(cssSelector)).sendKeys(value);
	}
	
	
	public void clickAndSetByXpath(String xpath, String value){
		driver.findElement(By.xpath(xpath)).click();
		driver.findElement(By.xpath(xpath)).sendKeys(value);
	}
	
	
/*##################################################################################### CLICK AND CLEAR #################################################################################################*/
	
	public void clickAndClearByID(String id){
		driver.findElement(By.id(id)).click();
		driver.findElement(By.id(id)).clear();
	}
	
	
	public void clickAndClearByName(String name){	
		driver.findElement(By.name(name)).click();
		driver.findElement(By.name(name)).clear();
	}
	
	
	public void clickAndClearByCssSelector(String cssSelector){	
		driver.findElement(By.cssSelector(cssSelector)).click();
		driver.findElement(By.cssSelector(cssSelector)).clear();
	}
	
	
	public void clickAndClearByXpath(String xpath){
		driver.findElement(By.xpath(xpath)).click();
		driver.findElement(By.xpath(xpath)).clear();
	}	
	
	
/*#################################################################################### SELECT VALUE ####################################################################################################*/
	
	public void selectValueByID(String id, String value){
		Select select = new Select(driver.findElement(By.id(id))); 
		select.selectByValue(value);
	}	
	
	
	public void selectValueByName(String name, String value){
		Select select = new Select(driver.findElement(By.name(name))); 
		select.selectByValue(value);
	}
	
	
	public void selectValueByCssSelector(String cssSelector, String value){
		Select select = new Select(driver.findElement(By.cssSelector(cssSelector))); 
		select.selectByValue(value);
	}
	
	
	public void selectValueByXpath(String xpath, String value){
		Select select = new Select(driver.findElement(By.xpath(xpath))); 
		select.selectByValue(value);
	}
	
	
/*#################################################################################### SELECT INDEX ####################################################################################################*/	
	
	public void selectIndexByID(String id, int value){
		Select select = new Select(driver.findElement(By.id(id))); 
		select.selectByIndex(value);
	}
	
	
	public void selectIndexByName(String name, int value){
		Select select = new Select(driver.findElement(By.name(name))); 
		select.selectByIndex(value);
	}	
	
	
	public void selectIndexByCssSelector(String cssSelector, int value){
		Select select = new Select(driver.findElement(By.cssSelector(cssSelector))); 
		select.selectByIndex(value);
	}
	
	
	public void selectIndexByXpath(String xpath, int value){
		Select select = new Select(driver.findElement(By.xpath(xpath))); 
		select.selectByIndex(value);
	}
	
	
/*#################################################################################### GET SELECTED VALUE ####################################################################################################*/	
	

	public WebElement getSelectedValueByID(String id, String value){
		Select select = new Select(driver.findElement(By.id(id))); 
		select.selectByValue(value);
		WebElement selectedValue = (WebElement) select.getAllSelectedOptions();
		return selectedValue;
	}
	
	
	public WebElement getSelectedValueByName(String name, String value){
		Select select = new Select(driver.findElement(By.name(name))); 
		select.selectByValue(value);
		WebElement selectedValue = (WebElement) select.getAllSelectedOptions();
		return selectedValue;
	}
	
	
	public WebElement getSelectedValueByCssSelector(String cssSelector, String value){
		Select select = new Select(driver.findElement(By.cssSelector(cssSelector))); 
		select.selectByValue(value);
		WebElement selectedValue = (WebElement) select.getAllSelectedOptions();
		return selectedValue;
	}
	
	
	public WebElement getSelectedValueByXpath(String xpath, String value){
		Select select = new Select(driver.findElement(By.xpath(xpath))); 
		select.selectByValue(value);
		WebElement selectedValue = (WebElement) select.getAllSelectedOptions();
		return selectedValue;
	}
	
	
/*#################################################################################### GET TEXT FROM SELECTED OPTION ####################################################################################################*/	
	
	public String getTextFromSelectedOptionByID(String id){
		Select select = new Select(driver.findElement(By.id(id)));
		String optionReturned = select.getFirstSelectedOption().getText();
		return optionReturned;
	}
	
	public String getTextFromSelectedOptionByName(String name){
		Select select = new Select(driver.findElement(By.name(name)));
		String optionReturned = select.getFirstSelectedOption().getText();
		return optionReturned;
	}
	
	
	public String getTextFromSelectedOptionByCssSelector(String cssSelector){
		Select select = new Select(driver.findElement(By.cssSelector(cssSelector)));
		String optionReturned = select.getFirstSelectedOption().getText();
		return optionReturned;
	}
	
	
	public String getTextFromSelectedOptionByXpath(String xpath){
		Select select = new Select(driver.findElement(By.xpath(xpath)));
		String optionReturned = select.getFirstSelectedOption().getText();
		return optionReturned;
	}
	
	
/*################################################################################# MARK CHECKBOX #####################################################################################################*/
	
	public void markCheckboxByID(String id){	
		WebElement checkbox = driver.findElement(By.id(id));	
		if(!checkbox.isSelected()){
			checkbox.click();
		}
	}
	
	
	public void markCheckboxByName(String name){		
		WebElement checkbox = driver.findElement(By.name(name));	
		if(!checkbox.isSelected()){
			checkbox.click();
		}
	}
	
	
	public void markCheckboxByCssSelector(String cssSelector){
		WebElement checkbox = driver.findElement(By.cssSelector(cssSelector));	
		if(!checkbox.isSelected()){
			checkbox.click();
		}
	}
	
	
	public void markCheckboxByXpath(String xpath){
		WebElement checkbox = driver.findElement(By.xpath(xpath));	
		if(!checkbox.isSelected()){
			checkbox.click();
		}
	}	
	
	
/*################################################################################# RADIO BUTTON #####################################################################################################*/
	
	public void checkRadioButtonByID(int option, String id) {
		List<WebElement> radios = driver.findElements(By.id(id));
		if(option > 0 && option <= radios.size()) {
			radios.get(option - 1).click();
		}else{
			throw new NotFoundException("Option " + option + " not found");
		}
	}
	
	
	public void checkRadioButtonByName(int option, String name) {
		List<WebElement> radios = driver.findElements(By.name(name));
		if(option > 0 && option <= radios.size()) {
			radios.get(option - 1).click();
		}else{
			throw new NotFoundException("Option " + option + " not found");
		}
	}
	
	
	public void checkRadioButtonByCssSelector(int option, String cssSelector) {
		List<WebElement> radios = driver.findElements(By.cssSelector(cssSelector));
		if(option > 0 && option <= radios.size()) {
			radios.get(option - 1).click();
		}else{
			throw new NotFoundException("Option " + option + " not found");
		}
	}
	
	
	public void checkRadioButtonByXpath(int option, String xpath) {
		List<WebElement> radios = driver.findElements(By.xpath(xpath));
		if(option > 0 && option <= radios.size()) {
			radios.get(option - 1).click();
		}else{
			throw new NotFoundException("Option " + option + " not found");
		}
	}
	
	
/*############################################################################################ WAIT'S ###########################################################################################*/
	
	public void implicitWait(){
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	
	public void explicitWaitByID(String id){
		WebDriverWait wait = new WebDriverWait(driver, 10);  
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
	}
	
	
	public void explicitWaitByName(String name){
		WebDriverWait wait = new WebDriverWait(driver, 10);  
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
	}
	
	
	public void explicitWaitByCSSSelector(String cssSelector){
		WebDriverWait wait = new WebDriverWait(driver, 10);  
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
	}
	
	
	public void explicitWaitByXpath(String xpath){
		WebDriverWait wait = new WebDriverWait(driver, 10);  
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}
}