package br.com.scripts;

import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class Functions{
	
public WebDriver driver;

public Functions(WebDriver driver){
	this.driver = driver;
}
	
/*############################################################################### GERA��O DE CARACTERES #################################################################################################*/

	public String randomCharacters(){
		
		Random random = new Random();	
		
		String string = "ABCDEFGHIJKLMNOPQRSTUVYWXZ";
		String randomString = "";  
		int index = -1;  
	
		for( int i = 0; i < 9; i++ ) {  
		   index = random.nextInt( string.length() );  
		   randomString += string.substring( index, index + 1 );  
		}  
		
		return randomString;
	}
	
	
	public String randomNumbers(){

		  Random randomCPF = new Random();
		  
		  for (int iCPF = 0; iCPF < 10; iCPF++);
		  
		  String cpf = Integer.toString(randomCPF.nextInt(2147483647));	
		  String cpfFim = cpf+22;	 
		  
		  return cpfFim;	  
	}	
	
	
/*######################################################################################## COUNT ELEMENTS #################################################################################################*/
	
	public void countElementsByXpath(String fieldValue){
		
		List<WebElement> listOfElements = driver.findElements(By.xpath(fieldValue));
		
		int count = listOfElements.size();
		
		System.out.println("O campo "+ fieldValue +" cont�m "+ count +" elementos");
	}
	
	
/*############################################################################### RESULT OF TEST (BOOLEAN) #################################################################################################*/	
	
	public boolean result(int i, String idMessageConfirm, String field, String fieldValue){
		try{
			WebElement verifyElement;
			verifyElement = driver.findElement(By.id(idMessageConfirm));
			Assert.assertTrue(verifyElement.isDisplayed());
			System.out.println("+++Teste "+ i +" do campo "+ field +" passou: " + fieldValue);
			return true;
		}catch(AssertionError e){
			Assert.fail();
			System.err.println("---Teste "+ i +" do campo "+field+" n�o passou" + fieldValue);
		}
		return false;
	}	
}