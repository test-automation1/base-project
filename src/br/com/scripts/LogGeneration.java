package br.com.scripts;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class LogGeneration {

static final Logger logger = LogManager.getLogger(LogGeneration.class.getName());
public Long startTime = System.currentTimeMillis();		

	public void logInitProject(String projectName){
		//imprime no arquivo de log
		logger.info("################################################################################# "+ projectName + " ########################################################################################################################");
		logger.info("");
		
		//imprime no Console
		System.out.println("########################################################################### "+ projectName + " ########################################################################################################################");
	}
	
	

	public void logInitCase(WebDriver driver, String caseName){
		
	
		System.out.println("==============================================="
				+ "\nInicializando "+caseName +" test");
		
	/*	System.out.println("Running.");
		
		System.out.println(startTime);
		if(startTime.equals(100)){
			System.out.println("Running..");
		}
		
		if(startTime.equals(200)){
			System.out.println("Running..");
		}*/

		
		//Imrprime no arquivo de log. 
		logger.info("--------------------------------------------------------------------------"+caseName+"-------------------------------"
					+ "---------------------------------------------------------------------------------------------------------------------");
		
		//imprime no Console
		System.out.println("\n--------------------------------------------------------------------------"+caseName+"-----------------------"
					+ "---------------------------------------------------------------------------------------------");
	}
	
	
	public boolean logCasePass(String fieldName, int interactions, boolean result){	
		
		if(result == true){
			System.out.println(" "+ interactions +  fieldName +" [+Pass]");
			logger.info(" "+ interactions +  fieldName +" [+Pass]"); 
		}
	
		
		if(result == false){	   
			System.out.println(" "+ interactions +  fieldName +"[+N�o passou]");
			logger.info("Teste " + interactions + " do campo "+ fieldName +" [+N�o passou]");
		}
		return false;			
	}
	
	
	/*public boolean logCaseNotPass(String fieldName, int interactions){
	
	}*/
	
	
	public void fieldWithJavaScriptDisabled(String fieldName){
		
		System.out.println("\n------------------ "+fieldName+" com Javascript desabilitado --------------------");
		
		logger.info("");		
		logger.info("------------------------------------------------------------------------------------------"
		+fieldName+" com Javascript desabilitado "
				  + " -----------------------------------------------------------------------------------------");
	}
	
	
	public void logInformacoesExecucao(String field, int hits, int errors, int hitsWithJS, int errorsWithoutJS, long startTime){
		
		long EndTime = System.currentTimeMillis(); 
		
		/*######################################### Imprimir no Console #####################################*/
		System.out.println("\n-------------------------Resultados do teste de "+field+"--------------------------\n"
				+ "Total de vezes que enviou com Javascript habilitado: " + hits + "\n"
				+ "Total de vezes que n�o enviou com Javascript habilitado: " + errors);
		
		
		System.out.println("\nTotal de vezes que enviou com Javascript desabilitado: " + hitsWithJS + "\n"
				+ "Total de vezes que n�o enviou com Javascript desabilitado: " + errorsWithoutJS);
		
		int submited = hits + hitsWithJS;
		int notSubmited = errors + errorsWithoutJS;
		
		System.out.println("\nTotal de vezes que enviou: " + submited + "\n"
				+ "Total de vezes que n�o enviou: "	+ notSubmited + "\n\n" 							
				+ "Tempo total de execu��o: " + new SimpleDateFormat("mm:ss").format(new Date(EndTime - startTime))+"\n"	
				+ "----------------------------------------------------------------------------");
		
		
		/*########################################### Imprimir no Log #######################################*/
		logger.info("");
		logger.info("------------------------------------------------------------------------------Resultados do teste de "+field+"----------------------------------------------------------------------------------------------------------------------");
		logger.info("Total de vezes que enviou com Javascript habilitado: " + hits);
		logger.info("Total de vezes que n�o enviou com Javascript habilitado: " + errors);
		
		logger.info("");
		
		logger.info("Total de vezes que enviou com Javascript desabilitado: " + hitsWithJS);
		logger.info("Total de vezes que n�o enviou com Javascript desabilitado: " + errorsWithoutJS);
		
		logger.info("");	
		logger.info("Total de vezes que enviou: " + submited);
		logger.info("Total de vezes que n�o enviou: "	+ notSubmited);  	
		
		logger.info("");		
		logger.info("Tempo total de execu��o: " + new SimpleDateFormat("mm:ss").format(new Date(EndTime - startTime)));
		logger.info("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	}	
	
	
	public static void relatorioFinal(long startTime){
		
		long EndTime = System.currentTimeMillis(); 		
		Date dataExecucao = GregorianCalendar.getInstance().getTime();  
        SimpleDateFormat format = new SimpleDateFormat();        
        
		System.out.println("\nData da execu��o: "+ format.format(dataExecucao) +"\n"
							+"Tempo total de execu��o de todos os testes: " + new SimpleDateFormat("mm:ss").format(new Date(EndTime - startTime))+"\n");		
		
		logger.info("Data da execu��o: "+ format.format(dataExecucao));
		logger.info("Tempo total de execu��o de todos os testes: " + new SimpleDateFormat("mm:ss").format(new Date(EndTime - startTime)));
		logger.info("####################################################################################################################################################################################################################");
	}
	
}
