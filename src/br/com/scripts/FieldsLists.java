package br.com.scripts;

public class FieldsLists {
	
	public String [] telephones(){
		String [] telephones = {
			//vazio (ERRO)
			"",
			//(ERRO)
			"9999-9999",
			//(ERRO)
			"9999-99999",
			//Letras e caracteres especiais (ERRO)
			"abc!@#%�&*",
			//Mais de 11 caracteres (ERRO)
			"4799999999991",
			//Formato (99) 9999-9999 (ACERTO)
			"9999999999",	
			////Formato (99) 99999-9999 (ACERTO)
			"99999999999"
			};
	return telephones;			
	}	
	
	
	public String [] valuesTxtFields (){
		String [] valuesTxtFields = {
			//vazio(ERRO)
			"",
			//50(ACERTO)
			"Lorem Ipsum � simplesmente uma simula��o de texto ",
			//100 (ACERTO)
			"Lorem Ipsum � simplesmente uma simula��o de texto da ind�stria tipogr�fica e de impressos, e vem sen",
			//200 (ERRO)
			"Lorem Ipsum � simplesmente uma simula��o de texto da ind�stria tipogr�fica e de impressos, e vem sendo utilizado desde o s�culo XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os em",
			//300 (ERRO)
			"Lorem Ipsum � simplesmente uma simula��o de texto da ind�stria tipogr�fica e de impressos, e vem sendo utilizado desde o s�culo XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu n�o s� a cinco s�culos, com",
			//Preencher o campo com letras, n�meros e caracteres (ACERTO)
			"098786765443231@#$%%�*(*&�%$asdasd"
		};
	return valuesTxtFields;	
	}	
	
	
	public String [] emails(){
		String [] emails = {
			//(ERRO)		
			"",
			//(ERRO)
			"nome",
			//(ERRO)
			"nome.sobrenome",
			//(ERRO)
			"#$%$*!@mail",
			//(ACERTO)
			"nome_!$#%@mail.com",
			//(ACERTO)
			"nome@mail",
			//(ACERTO)
			"nome.sobrenome@mail",
			//(ACERTO)
			"nome.sobrenome@mail.com",
			//(ACERTO)
			"nome.sobrenome@mail.com.br",
			//(ACERTO)
			"nome@mail.com",
			//(ACERTO)
			"nome_sobrenome@mail",
			//(ACERTO)
			"nome_sobrenome@mail.com",
			//(ACERTO)
			"nome_sobrenome@mail.com.br",
			//(ACERTO)
			"nome_sobrenome_outrosobrenome@mail",
			//(ACERTO)
			"nome_sobrenome_outrosobrenome@mail.com",
			//(ACERTO)
			"nome_sobrenome_outrosobrenome@mail.com.br",
		};
	return emails;
	}
	
	
	public String [] cpfs(){
		String [] cpfs = {
			//vazio(ERRO)
			"",
			//Letras e caracteres especiais (ERRO)
			"asdj!@#!$%",
			//CPF inv�lido (ERRO)
			"03121883989",
			//Preencher com mais de 11 caracteres
			"12345678912345",
			//CPF v�lido (ACERTO)
			"87423278815"
		};
	return cpfs;
	}	
	
	
	public String [] datas (){
		String [] datas = {
			//vazio(ERRO)
			"",
			//Formato MM/DD/AAAA (ERRO)
			"11/22/1994",
			//Letras e Caracteres especiais (ERRO)
			"ias!@#%$$",
			//Preencher com mais de 8 caracteres
			"123456789125",
			//Formato MM/DD/AAAA (ERRO)
			"22/11/1994"				
		};
	return datas;
	}
	
	
	public String [] ceps(){
		String [] ceps = {
			//vazio (ERRO)
			"",
			//Letras e n�meros(ERRO)
			"ias!@#%$$",
			//Mais de 8 caracteres (ERRO)
			"123456789",
			//Formato correto(ERRO)
			"99999-999"					
		};
	return ceps;
	}
	
	
	public String [] passwords(){
		String [] passwords = {
			//vazio(ERRO)
			"",
			//50(ACERTO)
			"Lorem Ipsum � simplesmente uma simula��o de texto ",
			//100 (ACERTO)
			"Lorem Ipsum � simplesmente uma simula��o de texto da ind�stria tipogr�fica e de impressos, e vem sen",
			//200 (ERRO)
			"Lorem Ipsum � simplesmente uma simula��o de texto da ind�stria tipogr�fica e de impressos, e vem sendo utilizado desde o s�culo XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os em",
			//300 (ERRO)
			"Lorem Ipsum � simplesmente uma simula��o de texto da ind�stria tipogr�fica e de impressos, e vem sendo utilizado desde o s�culo XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu n�o s� a cinco s�culos, com",
			//Preencher o campo com letras, n�meros e caracteres (ACERTO)
			"098786765443231@#$%%�*(*&�%$asdasd"				
		};
	return passwords;
	}
}