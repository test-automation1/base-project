package br.com.scripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BasePage {
	
	protected static WebDriver driver;
	
	public BasePage(WebDriver driver) {
		BasePage.driver = driver;
	}
	
	public BasePage() {
		BasePage.driver = new ChromeDriver();
		driver.manage().window().maximize();
	}
	
	public void get (String url){
		driver.get(url);
	}
	
	public void navigateTo(String url) {
		driver.navigate().to(url);
	}
	
	public WebDriver getDriver() {
		return driver;
	}
	
	public void closeNavegator() {
		getDriver().close();
	}
}
