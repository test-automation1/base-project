package br.com.projectname.fields;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import br.com.projectname.objects.ObjectExample;
import br.com.scripts.BrowserInteractions;

public class FieldsExample {

	 public WebDriver driver;	
	 public ObjectExample obj;
	 public BrowserInteractions interactions;

	public FieldsExample (WebDriver driver, Object object){
		this.driver = driver;
		this.obj = (ObjectExample) object;
		interactions = new BrowserInteractions(driver);
	}

	public void step1(){
		try{
			interactions.clickAndSetByID("", obj.getAtrribute1());
			interactions.clickAndSetByID("", obj.getAtrribute2());
			
			driver.findElement(By.id("")).submit();
			interactions.implicitWait();
			
		}catch(WebDriverException e){
			System.err.println("Element not found in step 1");
		}
	}
	
	public void step2(){
		try{
			interactions.selectIndexByID("", obj.getAtrribute3());
			
			driver.findElement(By.id("")).submit();
			interactions.implicitWait();
			
		}catch(WebDriverException e){
			System.err.println("Element not found in step 2");
		}
	}
}
