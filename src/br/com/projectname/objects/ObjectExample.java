package br.com.projectname.objects;

public class ObjectExample {

	public String atrribute1;
	public String atrribute2;
	public int atrribute3;
	
	public String getAtrribute1() {
		return atrribute1;
	}
	public void setAtrribute1(String atrribute1) {
		this.atrribute1 = atrribute1;
	}
	public String getAtrribute2() {
		return atrribute2;
	}
	public void setAtrribute2(String atrribute2) {
		this.atrribute2 = atrribute2;
	}
	public int getAtrribute3() {
		return atrribute3;
	}
	public int setAtrribute3(int atrribute3) {
		this.atrribute3 = atrribute3;
		return atrribute3;
	}
}
