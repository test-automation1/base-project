package br.com.projectname.main;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.projectname.tests.TestExample;
import br.com.scripts.BasePage;

public class ExampleMain extends BasePage{

TestExample test;
	
	@Before
	public void initConfigs(){
		TestExample test = new TestExample(driver);	
		test.openLink("");
		test.instanceObjects();
	}
	
	@Test public void testField1(){test.testField1();}
	@Test public void testField2(){test.testField2();}
	@Test public void testField3(){test.testField3();}
	
	@After
	public void finishTests(){
		driver.quit();
		driver.close();		
	}
}
