package br.com.projectname.tests;

import org.openqa.selenium.WebDriver;

import br.com.projectname.fields.FieldsExample;
import br.com.projectname.objects.ObjectExample;
import br.com.scripts.FieldsLists;
import br.com.scripts.Functions;
public class TestExample {

public WebDriver driver;
public ObjectExample obj;
public Functions functions;
public FieldsExample fields;
public FieldsLists fieldsLists = new FieldsLists();	
private static final long serialVersionUID = 1L;
private String url;
StringBuilder sb;
	
	public TestExample(WebDriver driver){
		this.driver = driver;
	}	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public void openLink(String url){
		this.url = url;
		driver.get(url);
	}
	
	public void instanceObjects(){
		try{
			ObjectExample obj = new ObjectExample();	
			
			obj.setAtrribute1("");
			obj.setAtrribute2("");
			obj.setAtrribute3(1);
			this.obj = obj;

			fields = new FieldsExample(driver, obj);
			functions = new Functions(driver);
			
		}catch(NullPointerException e){
			System.err.println("Object not instantiated: " + e);
		}		
	}
	
/*######################################################################################## STEP 1 #################################################################################################*/
	
	public void testField1(){	
		try{
			String [] telephones = fieldsLists.telephones();
			
			for(int i = 0; i <= telephones.length; i++){
				obj.setAtrribute1(telephones[i]);
				fields.step1();
				functions.result(i, "messageReturned", "testName", obj.getAtrribute1());
				driver.navigate().to(url);
			}
			obj.setAtrribute1(telephones[1]); //fixed number of array, for the next use test
		}catch(NullPointerException e){
			System.err.println("Object not instantiated: " + e);
			e.printStackTrace();
		}		
	}
	
	public void testField2(){	
		try{
			String [] telephones = fieldsLists.telephones();
			
			for(int i = 0; i <= telephones.length; i++){
				obj.setAtrribute2(telephones[i]);
				fields.step1();
				functions.result(i, "messageReturned", "testName", obj.getAtrribute1());
				driver.navigate().to(url);
			}
			obj.setAtrribute2(telephones[1]); //fixed number of array, for the next use test
		}catch(NullPointerException e){
			System.err.println("Object not instantiated: " + e);
			e.printStackTrace();
		}		
	}
	
/*######################################################################################## STEP 2 #################################################################################################*/

	public void testField3(){	
		try{
			int [] fieldsArray3= new int[10];
			
			for(int i = 0; i <= fieldsArray3.length; i++){
				obj.setAtrribute3(fieldsArray3[i]);
				fields.step2();
				functions.result(i, "messageReturned", "testName", obj.getAtrribute1());
				driver.navigate().to(url);
			}
			obj.setAtrribute3(fieldsArray3[1]); //fixed number of array, for the next use test
		}catch(NullPointerException e){
			System.err.println("Object not instantiated: " + e);
			e.printStackTrace();
		}		
	}
}
