# README #

This is a project model, just clone the repository, change names and configurations according to your project. It uses Selenium and JUnit to create and run your tests, the libraries are included.

# Step 1: Open project and change to real name #

* Project name;
* Packages names (replace only projectname);
* Class names;

Replace the word **Example** for your solution

## MainExample ##
### package: br.com.projectname.main.MainExample ###
Class performing all tests, you can execute your tests only in this class or create multiple main for each section or group. 
In this class (and only here) are presents anotations JUnit: Before, Test and After.

## ObjectExample ##
### package: br.com.projectname.objects.ObjectExample###

Listar todos os campos do formulário como atributos, para que os valores sejam setados e obtidos através destes atributos.
Todos os campos devem ser string. Exceção para campos select, que são selecionados por value.

Classe de campos - pacote: br.com.nomeprojeto.scripts.fieldsdoCadastro
Separar nesta classe todos os campos que serão utilizados no teste, isso para melhorar a legibilidade e estruturação do código e também para evitar redundância, já que será necessário preencher estes mesmos campos em todos os testes, então chama-se esta função ao invés de duplicar o código.

Criar classe para agrupar os Lists do formulário, se necessário.

Criar script de test
setar valor
chamar função que seta nos campos
chamar do resultado
navegar para URL; 

Exemplo: